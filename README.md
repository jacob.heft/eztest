# ezLandlordForms Coding Challenge

The following endpoints are implemented in this Web API project:

* Customer/GetById
* Customer/GetAll
* Customer/GetCustomersWithInvalidEmailAddresses

---

## Instructions: please make the following changes and then submit your work for review!

---

### 1. Properly identify invalid email addresses.

---

Currently, our code is considering too many email addresses to be valid!

For ANY email address in our system to be considered VALID, ALL of the following must be true:

* END in `.com`
* have EXACTLY one `@` symbol
* have AT LEAST one alphanumeric character to the LEFT of the `@` symbol
* have AT LEAST one alphanumeric character IN BETWEEN the `@` symbol and `.com`
* have NO other (non-alphanumeric) characters (for example, `/` or `` ` `` or `!`))

If any of the above conditions are not true, then the email address should be considered INVALID.

Please fix the `Customer/GetCustomersWithInvalidEmailAddresses` endpoint to abide by the above logic instead of whatever it is doing now!

Example VALID email addresses:
* `a@b.com`
* `example@website.com`

Example INVALID email addresses:
* `@website.com`
* `example@`
* `****@____.com`

---

### 2. Implement the EmployeeController.

---

The following endpoints are currently stubbed out, but not yet implemented:

* Employee/GetById
* Employee/GetAll
* Employee/GetEmployeesWithInvalidEmailAddresses

Please implement all three!

**Note: employees and customers should always have the SAME rules for what makes an email address valid or invalid.**

---