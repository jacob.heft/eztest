﻿using System.Collections.Generic;
using System.Linq;
using ezTestApi.Endpoints.Customer.Models;
using Microsoft.AspNetCore.Mvc;

namespace ezTestApi.Controllers
{
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private static readonly Customer[] Customers = new[]
        {
            new Customer { Id = 1, Name = "Ahmed", Email = "ahmed@website.com", LastPurchase = "Book" },
            new Customer { Id = 2, Name = "Ben", Email = "benwebsite.com", LastPurchase = "Car" },
            new Customer { Id = 3, Name = "Carlos", Email = "carlos`@website.com", LastPurchase = "Computer" },
            new Customer { Id = 4, Name = "Dinesh", Email = "", LastPurchase = "Chair" },
            new Customer { Id = 5, Name = "Emily", Email = "emily@website/com", LastPurchase = "Lamp" },
            new Customer { Id = 6, Name = "Frank", Email = "frank@website.com", LastPurchase = "Toy" }
        };

        public CustomerController()
        {
        }

        [HttpGet]
        [Route("Customer/GetById")]
        public Customer GetById(int id)
        {
            return Customers.SingleOrDefault(c => c.Id == id);
        }

        [HttpGet]
        [Route("Customer/GetAll")]
        public IEnumerable<Customer> GetAll()
        {
            return Customers;
        }

        [HttpGet]
        [Route("Customer/GetCustomersWithInvalidEmailAddresses")]
        public IEnumerable<Customer> GetCustomersWithInvalidEmailAddresses()
        {
            return Customers.Where(c => !c.Email.Contains("@"));
        }
    }
}
