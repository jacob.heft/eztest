﻿using System;
using System.Collections.Generic;
using ezTestApi.Endpoints.Employee.Models;
using Microsoft.AspNetCore.Mvc;

namespace ezTestApi.Controllers
{
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private static readonly Employee[] Employees = new[]
        {
            new Employee { Id = 1, Name = "Antonio", Email = "antonio!website.com", JobTitle = "Associate" },
            new Employee { Id = 2, Name = "Bence", Email = "bence@website.com", JobTitle = "Vice President" },
            new Employee { Id = 3, Name = "Cedric", Email = "carlos@@website.com", JobTitle = "President" },
            new Employee { Id = 4, Name = "Dante", Email = "dante@website.com@website.com", JobTitle = "CEO" },
            new Employee { Id = 5, Name = "Emma", Email = "emma@website.com", JobTitle = "CTO" },
            new Employee { Id = 6, Name = "Fabio", Email = null, JobTitle = "COO" }
        };

        public EmployeeController()
        {
        }

        [HttpGet]
        [Route("Employee/GetById")]
        public Employee GetById(int id)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        [Route("Employee/GetAll")]
        public IEnumerable<Employee> GetAll()
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        [Route("Employee/GetEmployeesWithInvalidEmailAddresses")]
        public IEnumerable<Employee> GetEmployeesWithInvalidEmailAddresses()
        {
            throw new NotImplementedException();
        }
    }
}
